import asyncio
import logging

import aiogram
import aiohttp
from aiogram.fsm.storage.redis import DefaultKeyBuilder, RedisStorage
from aiogram.types import BotCommand as BC
from redis.asyncio.client import Redis as AsyncRedis

import routers
from settings import config


async def main():
    bot = aiogram.Bot(token=config.bot_token.get_secret_value())
    storage = RedisStorage(
        AsyncRedis(host=config.redis_host), key_builder=DefaultKeyBuilder(with_bot_id=True)
    )
    dp = aiogram.Dispatcher(storage=storage)
    dp.include_router(routers.main_menu.router)
    dp.include_router(routers.cart.router)
    dp.include_router(routers.catalog.router)
    dp.include_router(routers.faq.router)
    await bot.set_my_commands([
        BC(command="start", description="Главное меню"),
        BC(command="file", description="Получить id фото"),
    ])
    backend = aiohttp.ClientSession(
        base_url=config.backend_url,
        headers={"Content-Type": "application/json"},
    )
    await dp.start_polling(bot, backend=backend, allowed_updates=dp.resolve_used_update_types())


if __name__ == "__main__":
    # TODO: add pagination for categories ~ 30
    # TODO: excel order write ~ 30
    # TODO: broadcast ~ 30
    # TODO: api auth header

    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=config.log_level
    )
    asyncio.run(main())
