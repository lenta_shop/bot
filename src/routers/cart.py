"""
This module is resbonsible for creating/submitting order and handling successfull payment
"""
import uuid
import aiogram
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, LabeledPrice, PreCheckoutQuery
from aiogram.utils.keyboard import InlineKeyboardBuilder
import aiohttp
from cb_factories import CartFactory, ProductFactory, BackBtnFactory
from routers.main_menu import MAIN_MENU, MENU_TEXT
from settings import config
import api


router = aiogram.Router()


async def _get_cart_porducts(cart, backend) -> list[api.schemas.Product]:
    products = await api.Endpoint(backend, api.constants.PRODUCTS).list_()
    return [prod for prod in products if str(prod.id) in cart]


async def _get_cart_list_menu(cart, backend: aiohttp.ClientSession):
    products = await _get_cart_porducts(cart, backend)
    builder = InlineKeyboardBuilder()
    for prod in products:
        builder.button(
            text=f"{prod.title}: {cart[str(prod.id)]} x {prod.price}",
            callback_data=ProductFactory(action="detail", product_id=prod.id, quantity=cart[str(prod.id)])
        )
    return builder



@router.pre_checkout_query()
async def process_pre_checkout_query(pre_checkout_query: PreCheckoutQuery, bot: aiogram.Bot):
    await pre_checkout_query.answer(ok=True)
    #await bot.send_message(chat_id=pre_checkout_query.from_user.id, text=str(pre_checkout_query.invoice_payload))


@router.message(aiogram.F.successful_payment)
async def process_successful_payment(message: aiogram.types.Message, backend: aiohttp.ClientSession):
    # TODO: add shipping info
    rsp_status = await (
        api.Endpoint(backend, api.constants.ORDERS)
        .update_(message.successful_payment.invoice_payload, {"status": "paid"})
    )
    await message.answer(f"Заказ успешно оплачен. {rsp_status=}", reply_markup=MAIN_MENU)


@router.callback_query(CartFactory.filter(aiogram.F.action=="open"))
async def open_cart(query: CallbackQuery, callback_data: CartFactory, backend: aiohttp.ClientSession, state: FSMContext):
    data = await state.get_data()
    cart = data.get("cart")
    if cart is None or cart == dict():
        await query.answer("Корзина пуста")
        await query.message.delete()
        await query.message.answer(text=MENU_TEXT, reply_markup=MAIN_MENU)
        return

    builder = await _get_cart_list_menu(cart, backend)
    builder.button(text="В меню", callback_data=BackBtnFactory(level="to_menu"))
    builder.button(text="Продолжить покупки", callback_data=BackBtnFactory(level="to_catalog"))
    builder.button(text="Оформить заказ", callback_data=CartFactory(action="order"))
    builder.adjust(1, repeat=True)
    await query.message.delete()
    await query.message.answer("Корзина", reply_markup=builder.as_markup())


@router.callback_query(CartFactory.filter(aiogram.F.action=="order"))
async def create_order(query: CallbackQuery, callback_data: CartFactory, backend: aiohttp.ClientSession, state: FSMContext):
    data = await state.get_data()
    cart = data.get("cart")
    if cart is None or cart == dict():
        await query.answer("Корзина пуста")
        return

    products = await _get_cart_porducts(cart, backend)
    for prod in products:
        if cart[str(prod.id)] > prod.stock:
            await query.answer(
                f"Невозможно создать заказ. Для покупки доступно всего {prod.stock} шт.",
                show_alert=True)
            return

    total_sum = sum((p.price*cart[str(p.id)] for p in products))
    if total_sum < 100 or total_sum > 1000:
        await query.answer(f"Сумма ({total_sum}) должна быть от 100 до 1000 руб.")
        return

    # create unpaid order, clear cart, reserve stok?
    order = api.schemas.Order(
        user=query.from_user.id,
        status="pending",
        total=total_sum,
        items=[dict(product_pk=p.id, title=p.title, quantity=cart[str(p.id)]) for p in products]
    ) 
    created_order = await api.Endpoint(backend, api.constants.ORDERS).create_(order)
    await query.message.answer_invoice(
        title="Lenta",
        description="Оплата вашего заказа",
        payload=created_order.id,
        provider_token=config.ukassa_token.get_secret_value(),
        currency="RUB",
        prices=[LabeledPrice(label=f"{prod.title} x {cart[str(prod.id)]}",
                    amount=100*prod.price*cart[str(prod.id)])
                for prod in products],
        start_parameter="test",
        need_shipping_address=True,
    )
    await state.update_data(cart=dict())
