"""
This module displays main menu and handles start
"""
import logging
import aiogram
from aiogram import filters
from aiogram.types import CallbackQuery, Message
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.exceptions import TelegramBadRequest
import aiohttp
from cb_factories import CatalogFactory, CartFactory
import api
from settings import config


router = aiogram.Router()

MAIN_MENU = (
    InlineKeyboardBuilder()
    .button(text="Каталог", callback_data=CatalogFactory(id_=None))
    .button(text="Корзина", callback_data=CartFactory(action="open"))
    .button(text="FAQ", callback_data="faq")
    .adjust(1, repeat=True)
    .as_markup()
)
BACK_BTN_TEXT =  "⬅️Назад"
MENU_TEXT = "Главное меню"


@router.message(filters.command.Command("start"))
async def cmd_start(message: Message, bot: aiogram.Bot, backend: aiohttp.ClientSession):
    channel = await bot.get_chat(config.channel)
    markup = (
        InlineKeyboardBuilder()
        .button(text="Подписаться", url=channel.invite_link)
        .button(text="✅ Я подписался", callback_data="check_subscription")
        .as_markup()
    )
    text = "Чтобы пользоваться ботом, поднишитесь на наш канал и нажмите \"Я подписался\""
    await message.answer(text, reply_markup=markup)
    user = api.schemas.TelegramUser(tg_id=message.from_user.id, name=message.from_user.username)
    await api.Endpoint(backend, api.constants.TGUSERS).create_(user)


@router.callback_query(aiogram.F.data=="check_subscription")
async def check_subscription(callback: CallbackQuery, bot: aiogram.Bot):
    try:
        chat_member = await bot.get_chat_member(
                chat_id=config.channel,
                user_id=callback.from_user.id)
        assert chat_member.status != "left"
    except (TelegramBadRequest, AssertionError):
        await callback.answer(f"Вы еще не подписались", show_alert=True)
        return
    await callback.answer()
    await callback.message.edit_text(MENU_TEXT, reply_markup=MAIN_MENU) 


@router.callback_query(aiogram.F.data=="to_menu")
async def main_menu(callback: CallbackQuery):
    await callback.message.edit_text(MENU_TEXT, reply_markup=MAIN_MENU)


@router.message(filters.Command("file"))
async def get_file_id(message: Message, bot: aiogram.Bot):
    await message.answer(str(message.photo.pop().file_id))
