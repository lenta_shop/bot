"""
This module is responsible for showing FAQ menu
"""
import aiogram
from aiogram.filters import callback_data
from aiogram.types import CallbackQuery
from aiogram.filters.callback_data import CallbackData
from aiogram.utils.keyboard import InlineKeyboardBuilder
import aiohttp
import api

from cb_factories import BackBtnFactory
from .main_menu import BACK_BTN_TEXT


class FAQFactory(CallbackData, prefix="faq_display"):
    id: int

#
#FAQ = [
#        {"question": "Test", "answer": "Answer 1"},
#        {"question": "Test 2", "answer": "Answer 2"},
#        {"question": "Test 3 long long question. Really long. Border will be crossed. No way it could fit", "answer": "Answer 3"},
#]
#



router = aiogram.Router()

@router.callback_query(aiogram.F.data=="faq")
async def list_faq(callback: CallbackQuery, backend: aiohttp.ClientSession):

    builder = InlineKeyboardBuilder()
    faqs = await api.Endpoint(backend, api.constants.FAQ).list_()
    for faq in faqs:
        builder.button(text=faq.question, callback_data=FAQFactory(id=faq.id))
    builder.button(text=BACK_BTN_TEXT, callback_data=BackBtnFactory(level="to_menu"))
    builder.adjust(1, repeat=True)

    await callback.message.edit_text(
        text="FAQ menu",
        reply_markup=builder.as_markup()
    )

@router.callback_query(FAQFactory.filter())
async def show_answer(callback: CallbackQuery, callback_data: FAQFactory, backend: aiohttp.ClientSession):
    await callback.answer()
    faq = await api.Endpoint(backend, api.constants.FAQ).get_(callback_data.id)
    await callback.message.edit_text(
        text=f"Q: {faq.question}\n\nA: {faq.answer}",
        reply_markup=callback.message.reply_markup
    )
