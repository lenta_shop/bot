"""
This module is responsible for browising shop catalog and adding items to cart
"""
from contextlib import suppress
import copy
import aiogram
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder
import aiohttp
from aiohttp.client import ClientSession
from settings import config
from .main_menu import BACK_BTN_TEXT, MAIN_MENU, MENU_TEXT
from cb_factories import CatalogFactory, BackBtnFactory, ProductFactory
import api
from .cart import open_cart


router = aiogram.Router()


def _get_catalog_menu(categories: list[api.schemas.Category], only_with_parent_id=None):
    builder = InlineKeyboardBuilder()
    for category in categories:
        if category.parent != only_with_parent_id:
            continue
        show_items = bool(only_with_parent_id is not None)
        builder.button(text=category.title, callback_data=CatalogFactory(id_=category.id, show_items=show_items))
    return builder


async def _get_products_list_menu(category_id, backend: aiohttp.ClientSession):
    products = await api.Endpoint(backend, api.constants.PRODUCTS).list_()
    products = (prod for prod in products if prod.category==category_id)
    builder = InlineKeyboardBuilder()
    for prod in products:
        builder.button(text=prod.title, callback_data=ProductFactory(action="detail", product_id=prod.id))
    return builder


def _get_product_markup(callback_data: ProductFactory):
    builder = InlineKeyboardBuilder()

    base_ = ProductFactory(product_id=callback_data.product_id, action="update", quantity=callback_data.quantity)
    decrease = copy.deepcopy(base_)
    decrease.quantity -= 1
    increase = copy.deepcopy(base_)
    increase.quantity += 1
    base_.action="to_cart"
    
    builder.button(text="-", callback_data=decrease)
    builder.button(text=str(callback_data.quantity), callback_data="pass")
    builder.button(text="+", callback_data=increase)
    builder.button(text="Подтвердить", callback_data=base_)

    builder.adjust(3, 1)
    return builder.as_markup()


@router.callback_query(CatalogFactory.filter())
async def browse_catalog(query: CallbackQuery, callback_data: CatalogFactory, backend: aiohttp.ClientSession):
    if callback_data.show_items:
        text = "Выберите нужный товар"
        builder = await _get_products_list_menu(callback_data.id_, backend)
    else:
        text = "Выберите нужную категорию"
        categories = await  api.Endpoint(backend, api.constants.CATEGORIES).list_()
        builder = _get_catalog_menu(categories, callback_data.id_)
    back_level = "to_catalog" if callback_data.id_ else "to_menu"
    builder.add(InlineKeyboardButton(text=BACK_BTN_TEXT, callback_data=BackBtnFactory(level=back_level).pack()))
    builder.adjust(1, repeat=True)
    await query.message.edit_text(
        text=text,
        reply_markup=builder.as_markup()
    )


@router.callback_query(ProductFactory.filter(aiogram.F.action=="detail"))
async def select_product(query: CallbackQuery, callback_data: ProductFactory, backend: aiohttp.ClientSession):
    await query.message.delete()
    product = await api.Endpoint(backend, api.constants.PRODUCTS).get_(callback_data.product_id)
    text = str(
        f"Name: {product.title}"
        f"\n\nDescription: {product.description}"
        f"\n\nPrice: {product.price}"
    )
    markup = _get_product_markup(callback_data)
    # TODO: max captions size = 1024, may be not enouth for some descriptions
    await query.message.answer_photo(
        photo=product.tg_photo_id,
        caption=text,
        reply_markup=markup
    )
     


@router.callback_query(ProductFactory.filter(aiogram.F.action=="update"))
async def ajust_quantity(query: CallbackQuery, callback_data: ProductFactory, backend: aiohttp.ClientSession):
    await query.answer()
    callback_data.quantity = max(callback_data.quantity, 0)
    markup = _get_product_markup(callback_data)
    await query.message.edit_reply_markup(reply_markup=markup)


@router.callback_query(ProductFactory.filter(aiogram.F.action=="to_cart"))
async def add_to_cart(query: CallbackQuery, callback_data: ProductFactory, backend: ClientSession, state: FSMContext):
    data = await state.get_data()
    cart = data.get("cart")
    cart = cart if cart else dict()
    pk = str(callback_data.product_id)
    if callback_data.quantity == 0:
        with suppress(KeyError):
            del cart[pk]
    elif len(cart.keys()) >= config.max_cart_len and not cart.get(pk):
        await query.answer(text=f"Не более {config.max_cart_len} позиций в корзине", show_alert=True) 
    else:
        cart[pk] = callback_data.quantity
    

    await state.update_data(cart=cart)
    await open_cart(query, callback_data, backend, state) 


@router.callback_query(BackBtnFactory.filter())
async def handle_back_click(query: CallbackQuery, callback_data: BackBtnFactory, state: FSMContext, backend: aiohttp.ClientSession):
    match callback_data.level:
        case "to_menu":
            await query.message.edit_text(MENU_TEXT, reply_markup=MAIN_MENU)
        case "to_catalog":
            await browse_catalog(query, CatalogFactory(id_=None), backend)
