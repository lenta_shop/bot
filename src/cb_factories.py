from aiogram.filters.callback_data import CallbackData


class CatalogFactory(CallbackData, prefix="browse_catalog"):
    id_: int | None = None
    show_items: bool = False


class ProductFactory(CallbackData, prefix="product_selection"):
    action: str
    product_id: int
    quantity: int = 1


class BackBtnFactory(CallbackData, prefix="back"):
    level: str
    category: str | None = None


class CartFactory(CallbackData, prefix="manage_cart"):
    action: str
