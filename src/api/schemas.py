import pydantic
from pydantic import UUID4



class Product(pydantic.BaseModel):
    id: int
    title: str
    price: int
    description: str
    tg_photo_id: str
    stock: int
    category: int | None = None
    

class Category(pydantic.BaseModel):
    id: int
    title: str
    parent: int | None = None
    #created: str


class Order(pydantic.BaseModel):
    id: str | None = None
    status: str
    user: int
    total: int
    items: list[dict]
    #created: str
    #updated: str


class TelegramUser(pydantic.BaseModel):
    tg_id: int | None = None
    name: str | None = None


class FAQ(pydantic.BaseModel):
    id: int | None
    question: str
    answer: str

