from aiohttp import ClientSession
from . import constants
from .schemas import FAQ, Product, Category, TelegramUser, Order


MODEL_MAP = {
    constants.PRODUCTS: Product,
    constants.CATEGORIES: Category,
    constants.TGUSERS: TelegramUser,
    constants.ORDERS: Order,
    constants.FAQ: FAQ,
}

class Endpoint():
    def __init__(self, backend: ClientSession, service_name: str) -> None:
        self.backend = backend
        self.service_name = service_name
        self.model = MODEL_MAP[service_name]

    async def list_(self):
        rsp = await self.backend.get(f"/api/{self.service_name}/")
        json_ = await rsp.json()
        return [self.model(**i) for i in json_["results"]]


    async def get_(self, pk):
        rsp = await self.backend.get(f"/api/{self.service_name}/{pk}/")
        json_= await rsp.json()
        return self.model(**json_)


    async def create_(self, obj):
        assert isinstance(obj, self.model)
        rsp = await self.backend.post(f"/api/{self.service_name}/", json=obj.dict())
        json = await rsp.json()
        return self.model(**json)
    

    async def update_(self, pk, data):
        rsp = await self.backend.patch(f"/api/{self.service_name}/{pk}/", json=data)
        return rsp.status
