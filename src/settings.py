from pydantic import BaseSettings, SecretStr


class Settings(BaseSettings):
    log_level: str = "DEBUG"
    bot_token: SecretStr
    ukassa_token: SecretStr
    shop_id: SecretStr
    shop_article_id: SecretStr
    max_cart_len: int = 2
    redis_host: str = "localhost"
    channel: str
    backend_url: str

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


config = Settings()

