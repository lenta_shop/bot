FROM python:3.10.8 AS venv

RUN curl -sSL https://install.python-poetry.org | python3.10 -
ENV PATH="/root/.local/bin:$PATH"

WORKDIR /app
COPY poetry.lock pyproject.toml ./

# The `--copies` option tells `venv` to copy libs and binaries
# instead of using links (which could break since we will
# extract the virtualenv from this image)
RUN python -m venv --copies /deps/venv \
  && . /deps/venv/bin/activate && poetry install

FROM python:3.10.8-slim
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
RUN apt-get update && apt-get install -y ffmpeg
COPY --from=venv /deps/venv /deps/venv/
ENV PATH /deps/venv/bin:$PATH

WORKDIR /app
COPY . ./
CMD ["python", "src/main.py"]
